# Vigenere_Cipher

[Vigenere_Cipher](https://gitlab.com/bamathis/Vigenere_Cipher) is an implemention of the Vigenere cipher for encryption and decryption.

## Quick Start

### Program Execution

#### Encoding
```
$ ./VigenereEncoder.cpp 
```

#### Decoding
```
$ ./VigenereDecoder.cpp 
```

### Known Issues

- The given input file must only consist of letters and spaces. 