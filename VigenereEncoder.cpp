#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

using namespace std;

void createCipherArray(char cipherArray[][26],char alphabet[]);
void getText(ifstream& input, string& plainText);
void encodeText(string& cipherText,char alphabet[],char cipherArray[][26],string key,int keyLength, string plainText,int& textLength);
void displayText(string cipherText,ofstream& output, int textLength);

int main ()
	{
	char alphabet[26] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char cipherArray[26][26];
	int keyLength = 0;
	string key;
	string inFileName;
	string outFileName;
	string cipherText;
	string plainText;
	ofstream output;
	ifstream input;
	int textLength;
	createCipherArray(cipherArray, alphabet);
	cout << "Enter key for encryption: ";
	getline(cin,key);
	cout << "Enter file name to be encrypted: ";
	getline(cin,inFileName);
	if(input.fail())
		{
		cout << "File not found" << endl;
		exit(1);
		}
	cout << "Enter output file name for encrypted text: ";
	getline(cin,outFileName);;
	for(int n = 0; key[n] != '\0'; n++)
		{
		if(!islower(key[n]))
			key[n] = key[n] - 'A' + 'a';
		keyLength = n;
		}
	keyLength++;
	input.open(inFileName.c_str());
	getText(input, plainText);
	input.close();
	encodeText(cipherText,alphabet,cipherArray,key,keyLength,plainText,textLength);
	output.open(outFileName.c_str());
	if(output.fail())
		{
		cout << "Cipher text not written to file" << endl;
		exit(1);
		}
	displayText(cipherText,output,textLength);
	output.close();
	}
//=====================================================
//Creates 26x26 Vigenere square
void createCipherArray(char cipherArray[][26],char alphabet[])
	{
	for(int n = 0; n < 26; n++)
		for(int m = 0; m < 26; m++)
			cipherArray[n][m]= alphabet[(n+m)%26];
	}
//=====================================================
//Reads input file into plainText string
void getText(ifstream& input, string& plainText)
	{
	plainText = "";
	string temp;
	while(getline(input,temp))
		plainText = plainText + temp;
	}
//=====================================================
//Uses the given plain text and key to produce cipher text
void encodeText(string& cipherText,char alphabet[],char cipherArray[][26],string key,int keyLength, string plainText,int& textLength)
	{
	cipherText = "";
	int m = 0;
	textLength = 0;
	for(int n = 0; plainText[n] != '\0'; n++)
		{
		if(!islower(plainText[n]))
			plainText[n] = plainText[n] - 'A' + 'a';
		if(plainText[n] >= 'a' && plainText[n] <= 'z')
			{
			char temp;
			temp = cipherArray[key[m%keyLength]-'a'][plainText[n]-'a'];
			cipherText = cipherText + temp;
			textLength++;
			m++;
			}
		}
	cipherText = cipherText + '\0';
	}

//=======================================================
//Displays the cipher text and writes it to a file
void displayText(string cipherText,ofstream& output, int textLength)
	{
	cout << endl;
	for(int n = 0; n < textLength; n++)
		{
		cout << cipherText[n];
		output << cipherText[n];
		if((n+1) % 5 == 0)
			{
			cout << " ";
			output << " ";
			}
		if((n+1)%20 == 0)
			{
			cout << endl;
			output << endl;
			}
		if((n+1) % 100 == 0)
			{
			cout << endl;
			output << endl;
			}
		}
	cout << endl;
	}