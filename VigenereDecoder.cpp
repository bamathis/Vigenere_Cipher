#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>
#include <iomanip>

using namespace std;

void createCipherArray(char cipherArray[][26],char alphabet[]);
void getText(ifstream& input, string& cipherText);
void getKeyLength(int frequency[],float& coincidence,string cipherText,int& textLength,int& keyLength);
string guessKey(int keyLength,string cipherText, int keyAnalysis[][26],char cipherArray[][26]);
void decodeText(string cipherText,char alphabet[],char cipherArray[][26],string& key,int& keyLength, string& plainText,int textLength, bool & solved);
void displayAnalysis(string key,int keyLength, float coincidence, int keyAnalysis[][26], ostream& stream);
void displayText(string plainText, int textLength,ostream& stream);

int main ()
	{
	int frequency[26] = {0};
	char alphabet[26] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	char cipherArray[26][26];
	int keyLength;
	int guessedKeyLength;
	bool solved;
	char displayChoice;
	char analysisChoice;
	string key;
	string fileName;
	string cipherText;
	string plainText;
	ifstream input;
	int textLength;
	float coincidence;
	createCipherArray(cipherArray, alphabet);
	cout << "Enter file name to be decrypted: ";
	getline(cin,fileName);
	input.open(fileName.c_str());
	if(input.fail())
		{
		cout << "File not found " << endl;
		exit(1);
		}
	getText(input, cipherText);
	input.close();
	solved = false;
	getKeyLength(frequency, coincidence,cipherText,textLength,guessedKeyLength);
	keyLength = guessedKeyLength;
	int keyAnalysis[keyLength][26] ={0};
	key = guessKey(guessedKeyLength,cipherText,keyAnalysis,cipherArray);
	while(!solved)
		decodeText(cipherText,alphabet,cipherArray,key,keyLength,plainText,textLength,solved);
	cout << "Would you like to display or save output(D/S)? ";
	cin >> displayChoice;
	cout << "Would you like to include frequency analysis(Y/N)? ";
	cin >> analysisChoice;
	if(displayChoice == 'D' && analysisChoice == 'Y')
		{
		cout << endl;
		displayAnalysis(key,guessedKeyLength,coincidence,keyAnalysis,cout);
		displayText(plainText,textLength,cout);
		}
	else if(displayChoice == 'D'&& analysisChoice == 'N')
		{
		cout << endl;
		displayText(plainText,textLength,cout);
		}
	else if(displayChoice == 'S'&& analysisChoice == 'Y')
		{
		ofstream output;
		cin.ignore(999,'\n');
		cout << "Enter name to save file as: ";
		getline(cin,fileName);
		output.open(fileName.c_str());
		displayAnalysis(key,guessedKeyLength,coincidence,keyAnalysis,output);
		displayText(plainText,textLength,output);
		output.close();
		}
	else if(displayChoice == 'S'&& analysisChoice == 'N')
		{
		ofstream output;
		cin.ignore(999,'\n');
		cout << "Enter name to save file as: ";
		getline(cin,fileName);
		output.open(fileName.c_str());
		displayText(plainText,textLength,output);
		output.close();
		}
	}
//=====================================================
//Creates 26x26 Vigenere square
void createCipherArray(char cipherArray[][26],char alphabet[])
	{
	for(int n = 0; n < 26; n++)
		for(int m = 0; m < 26; m++)
			cipherArray[n][m]= alphabet[(n+m)%26];
	}
//=====================================================
//Reads input file into cipherText string
void getText(ifstream& input, string& cipherText)
	{
	cipherText = "";
	string temp;
	while(getline(input,temp))
		cipherText = cipherText + temp;
	}
//=======================================================
//Determines length of the key using Friedman test
void getKeyLength(int frequency[],float& coincidence,string cipherText,int& textLength,int& keyLength)
	{
	float tempLength;
	textLength = 0;
	coincidence = 0;
	for(int n = 0; cipherText[n] != '\0'; n++)
		{
		if(cipherText[n] > 32)
			{
			frequency[cipherText[n] - 'A']++;
			textLength++;
			}
		}
	for(int n = 0; n < 26; n++)
		coincidence = coincidence +  frequency[n]*frequency[n] - frequency[n];
	coincidence = coincidence / (textLength*(textLength - 1));
	tempLength = 0.027 * textLength/((textLength-1)*coincidence + 0.065 - 0.038 * textLength);
	keyLength = ceil(tempLength);
	}
//========================================================
//Determines possible key by using frequence analysis
string guessKey(int keyLength,string cipherText, int keyAnalysis[][26],char cipherArray[][26])
	{
	int m;
	string key;
	int maxNumber;
	int maxPosition;
	m = 0;
	key = "";
	for(int n = 0; cipherText[n] != '\0'; n++)
		{
		if(cipherText[n] > 32)
			{
			keyAnalysis[m%keyLength][cipherText[n]-'A']++;
			m++;
			}
		}
	cout << endl;
	for(int n = 0; n < 26; n++)
		cout << setw(3) << cipherArray[0][n];
	cout << endl;
	for(int n = 0; n < keyLength; n++)
		{
		maxNumber = 0;
		maxPosition = 0;
		for(int l = 0; l < 26; l++)
			{
			cout << setw(3) << keyAnalysis[n][l];
			if(keyAnalysis[n][l] > maxNumber)
				{
				maxNumber = keyAnalysis[n][l];
				maxPosition = l;
				}
			}
		cout << endl;
		for(int k = 0; k < 26; k++)
			if(cipherArray['E' - 'A'][k] - 'A' == maxPosition)
				{
				key = key + (char)('a' + k);
				k = 26;
				}
		}
	cout << endl << "The current guess for the key is " << key << endl << endl;
	return key;
	}
//=====================================================
//Uses the given cipher text to try to guess the plain text
void decodeText(string cipherText,char alphabet[],char cipherArray[][26],string& key,int& keyLength, string& plainText,int textLength, bool & solved)
	{
	char userAnswer;
	plainText = "";
	int m = 0;
	for(int n = 0; cipherText[n] != '\0'; n++)
		{
		if(cipherText[n] > 32)
			{
			for(int l = 0; l < 26; l++)
				{
				if (cipherArray[key[m%keyLength]-'a'][l] == cipherText[n])
					{
					char temp;
					temp = (l + 'a');
					plainText = plainText + temp;
					l = 26;
					}
				}
			m++;
			if(m == 20)
				{
				cout << "Sample of the text is: ";
				displayText(plainText,m,cout);
				cout << "Does the text look correct(Y/N)? ";
				cin >> userAnswer;
				if(userAnswer == 'Y')
					solved = true;
				else
					{
					cin.ignore(999,'\n');
					cout << "The current guess for key is " << key << " enter new key to use: ";
					getline(cin,key);
					for(keyLength = 0; key[keyLength] != '\0'; keyLength++)
						{
						if(!islower(key[keyLength]))
							key[keyLength] = key[keyLength] - 'A' + 'a';
						}
					cout << endl;
					return;
					}
				}
			}
		}
	plainText = plainText + '\0';
	if(m < 20)												//For cipher text less than 20 characters
		{
		displayText(plainText,m,cout);
		cout << "Does the text look correct(Y/N)? ";
		cin >> userAnswer;
		if(userAnswer == 'Y')
			solved = true;
		else
			{
			cin.ignore(999,'\n');
			cout << "The current guess for key is " << key << " enter new key to use: ";
			getline(cin,key);
			for(keyLength = 0; key[keyLength] != '\0'; keyLength++)
				{
				if(!islower(key[keyLength]))
					key[keyLength] = key[keyLength] - 'A' + 'a';
				}
			cout << endl;
			return;
			}
		}
	}
//=======================================================
//Displays the information from the frequency analysis
void displayAnalysis(string key,int keyLength, float coincidence, int keyAnalysis[][26], ostream& stream)
	{
	stream << "The key is: " << key << endl;
	stream << "The index of coincidence is: " << coincidence << endl;
	stream << "The character frequence by letter is " << endl << endl;
	for(int n = 0; n < 26; n++)
		stream << setw(3) << (char) (n + 'A');
	stream << endl;
	for(int n = 0; n < keyLength; n++)
		{
		for(int l = 0; l < 26; l++)
			stream << setw(3) << keyAnalysis[n][l];
		stream << endl;
		}
	stream << endl;
	}
//=======================================================
//Displays the plain text
void displayText(string plainText, int textLength,ostream& stream)
	{
	stream << endl;
	for(int n = 0; n < textLength; n++)
		{
		stream << plainText[n];
		if((n+1) % 5 == 0)
			stream << " ";
		if((n+1)%20 == 0)
			stream << endl;
		if((n+1) % 100 == 0)
			stream << endl;
		}
	stream << endl;
	}